PostgreSQL
=========  
  
A simple ansible role for PostgreSQL installation
  
Requirements  
------------  
  
Ansible installed  
  
Default Variables  
--------------  
  
**postgres_pass:** P@ssw0rd  

Dependencies  
------------  
  
None  
  
Example Playbook  
----------------  
  
    - hosts: servers  
      roles:  
         - { role: postgresql }  
  
License  
-------  
  
BSD  